/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusError>
#include <QCommandLineParser>
#include <QDebug>
#include "singleapplication.h"
#ifndef QT_DEBUG
#include "logger.h"
#endif
#include "processmanager.h"
#include "processmanagerservice.h"
#include "appmanagercgroupd.h"

int main(int argc, char *argv[])
{
#ifndef QT_DEBUG
    qInstallMessageHandler(common::Logger::outputMessage);
#endif
    SingleApplication app(argc, argv, true);

    QCommandLineParser parser;
    parser.setApplicationDescription("Kylin Process Manager");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption thawProcessesOption("t", QCoreApplication::translate("main", "Thaw all frozen processes"));
    parser.addOption(thawProcessesOption);
    parser.process(app);
    bool thawProcesses = parser.isSet(thawProcessesOption);
    qWarning() << "app.isSecondary())" <<  app.isSecondary() << thawProcesses;
    // If this is a secondary instance
    if (app.isSecondary()) {   
        if (thawProcesses) {
            app.sendMessage("thawProcesses");
        }
        return 0;
    }

    if (thawProcesses) {
        AppManagerCgroupd cgroupManager;
        cgroupManager.thawAllProcesses(true);
        return 0;
    }

    ProcessManager processManager;
    QDBusConnection connection = QDBusConnection::systemBus();
    if (!connection.registerService("com.kylin.ProcessManager") ||
            !connection.registerObject("/com/kylin/ProcessManager", &processManager)) {
        qWarning() << "register dbus service failed" << connection.lastError();
    }
    ProcessManagerService service(&processManager);
    Q_UNUSED(service)

    QObject::connect(&app, &SingleApplication::receivedMessage, &app, [&processManager] (quint32 instanceId, QByteArray message) {
        Q_UNUSED(instanceId)
        processManager.thawAllProcesses();
    });

    return app.exec();
}
