/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RESOURCEWATCHER_H
#define RESOURCEWATCHER_H

#include <QObject>
#include <QQueue>

class ResourceWatcher : public QObject
{
    Q_OBJECT
public:
    explicit ResourceWatcher(QObject *parent = nullptr);

public Q_SLOTS:
    virtual void start() = 0;
    virtual void stop() = 0;

signals:
    void finished();
    void ResourceThresholdWarning(const QString &resource, int level);

protected:
    bool thresholdWarning(int urgency);

protected:
    int m_detectEffectiveNumber;
    QQueue<int> m_detectValues;
};

#endif // RESOURCEWATCHER_H
