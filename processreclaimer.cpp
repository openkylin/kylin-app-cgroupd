/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "processreclaimer.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

ProcessReclaimer::ProcessReclaimer(QObject *parent)
    : QObject{parent},
      m_isAvailable(false)
{
    if (access("/proc/1/reclaim",F_OK) != -1) {
        m_isAvailable = true;
    }
}

bool ProcessReclaimer::isAvailable()
{
    return access("/proc/1/reclaim", F_OK) != -1;
}

common::DBusResult ProcessReclaimer::reclaimProcesses(const QList<int> pids)
{
    auto result = common::initDbusResult();
    if (pids.isEmpty()) {
        result[common::kDbusErrMsg] = "The pids is empty!";
        result[common::kDbusResult] = false;
        return result;
    }
    const char *content = "all";
    for (auto const &pid : qAsConst(pids)) {
        QString api = QString("/proc/%1/reclaim").arg(pid);
        QByteArray ba = api.toLocal8Bit();
        int fd = open(ba.data(), O_RDWR | O_NONBLOCK);
        if (fd < 0) {
            result[common::kDbusErrMsg] = "Open api file " + api + " failed, " + strerror(errno);
            result[common::kDbusResult] = false;
            return result;
        }
        if (write(fd, content, strlen(content) + 1) < 0) {
            result[common::kDbusErrMsg] = "Write api file " + api + " failed, " + strerror(errno);
            result[common::kDbusResult] = false;
            return result;
        }
    }
    result[common::kDbusResult] = true;
    return result;
}
