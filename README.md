# kylin-app-cgroupd介绍
## 简介
+  kylin-app-cgroupd是分级冻结的后端服务，为kylin-app-manager提供方法和信号，是实现分级冻结的核心模块。
+ kylin-app-cgroupd利用Control Groups技术(`https://docs.kernel.org/admin-guide/cgroup-v2.html`)创建应用进程分组或者删除应用进程分组，获取系统中支持限制进程的资源名称，设置分组中进程的资源使用配比。
+ kylin-app-cgroupd通过对/proc/pressure/cpu、/proc/pressure/io和/proc/meminfo文件进行监测，实现了对CPU、I/O、存储等系统资源监测预警的功能。
## 编译依赖
- `pkg-config`
-  `cmake`
-  `qtbase5-dev`
-  ` libcgroup-dev (>= 3.0.0-1)`
-  `libprocps-dev`
- `libspdlog-dev`

## 编译
```shell
mkdir build
cd build
cmake ..
make
```
## 使用要求
+ 当前系统需要支持cgroup v2版本，终端输入$ `mount | grep cgroup`，输出结果显示`cgroup2 on /sys/fs/cgroup`，说明支持cgroup v2系统。再输入$ `ll /sys/fs.cgroup`，输出结果有blick/、memory/和freezer/等目录结构的是cgroup v1结构，需要修改默认配置，$`sudo vim /etc/default/grub`,添加以下配置项 `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash systemd.unified_cgroup_hierarchy=1"`，然后输入$`sudo update-grub`， $`reboot`。
+ 重启后检验是否默认开启cgroup v2版本，终端输入$`ll /sys/fs/cgroup`，输出结果中显示`cgroup.controllers`和`cgroup.subtree_control`等系统文件的表明已开启cgroup v2结构。
+ kylin-app-cgroupd源码包地址：https://gitee.com/openkylin/kylin-app-cgroupd

## 配置文件
+ com.kylin.ProcessManager.conf   存放目录: /etc/dbus-1/system.d/
+ com.kylin.ProcessManager.service  存放目录: /usr/share/dbus-1/system-services/
+ configs/kylin-process-manager.service 存放目录: /lib/systemd/system/
+ kylin-process-manager-cleaner.service 存放目录: /lib/systemd/system/
+ com.kylin.ProcessManager.xml 存放目录: /usr/share/dbus-1/interfaces/
+ kylin-process-manager.json 存放目录: /etc/kylin-process-manager/

## 联系我们
- `https://gitee.com/openkylin/kylin-app-cgroupd`
