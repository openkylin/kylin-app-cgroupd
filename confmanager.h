/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONFMANAGER_H
#define CONFMANAGER_H

#include <QObject>
#include <QMap>

#define FEAT_FROZEN "frozen"

class QFileSystemWatcher;
class ConfManager : public QObject
{
    Q_OBJECT
public:

    enum Resource {
        CPU,
        IO,
        Memory
    };
    Q_ENUM(Resource);

    enum ResourceUrgency {
        Low = 1,
        Medium = 2,
        High = 4,
    };
    Q_ENUM(ResourceUrgency);

    typedef QMap<ResourceUrgency, int> ResourceUrgencyThrehold;

    explicit ConfManager(QObject *parent = nullptr);

    QStringList features() const;
    QList<Resource> pressureResouces() const;
    QMap<ResourceUrgency, QString> pressureTriggers(Resource resource) const;
    ResourceUrgencyThrehold resourceThreshold(Resource resource) const;
    int detectEffectiveNumber(Resource resource) const;
    int resourceUrgencyIndex(ResourceUrgency urgency) const;

private Q_SLOTS:
    void readConf();

private:
    void updateFeatures(const QJsonObject &jsonObj);
    void updateResourceConfig(const QString &resource,
                              const QString &api,
                              const QJsonArray &thresholds,
                              const int &effectiveNumber);

private:
    QStringList m_features;
    QMap<Resource, ResourceUrgencyThrehold> m_pressureTriggers;
    QMap<Resource, ResourceUrgencyThrehold> m_resourceThresholds;
    QMap<Resource, int> m_detectEffectiveNumbers;

    QFileSystemWatcher *confFileWatcher;
    const char *kConfFilename = "/etc/kylin-process-manager/kylin-process-manager.json";
};

#endif // CONFMANAGER_H
