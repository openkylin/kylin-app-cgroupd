/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "logger.h"
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <spdlog/spdlog.h>

namespace common {

Logger::Logger(QObject *parent) : QObject(parent) {}

void Logger::outputMessage(QtMsgType type,
                           const QMessageLogContext &context,
                           const QString &msg) {
    Q_UNUSED(context)
    switch (type) {
    case QtDebugMsg: {
      logger()->debug(msg.toStdString());
      break;
    }
    case QtWarningMsg: {
      logger()->warn(msg.toStdString());
      break;
    }
    case QtCriticalMsg: {
      logger()->critical(msg.toStdString());
      break;
    }
    case QtInfoMsg: {
      logger()->info(msg.toStdString());
      break;
    }
    default:
      break;
    }
    logger()->flush();
}

std::shared_ptr<spdlog::logger> Logger::logger() {
    QString logFileName = "/var/log/kylin-process-manager/daily.txt";
    static auto logger = spdlog::daily_logger_mt(
        "daily_logger", logFileName.toStdString().c_str(), 2, 30);
    logger->set_level(spdlog::level::info);
    return logger;
}

}  // namespace common
