/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "systemresourcewatcher.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTimer>
#include <QThread>
#include <QDebug>
#include <sys/epoll.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <proc/sysinfo.h>

#include "common.h"

SystemResourceWatcher::SystemResourceWatcher(QObject *parent)
    : ResourceWatcher{parent}
    , m_stop(false)
    , m_timer(new QTimer(this))
{
    connect(m_timer, &QTimer::timeout, this, &SystemResourceWatcher::detectMemory);
    ConfManager &confMgr = common::Singleton<ConfManager>::GetInstance();
    m_resourceThrehold = confMgr.resourceThreshold(ConfManager::Memory);
    m_detectEffectiveNumber = confMgr.detectEffectiveNumber(ConfManager::Memory);
    if (m_detectEffectiveNumber == 0) {
        m_detectEffectiveNumber = 5;
    }
}

void SystemResourceWatcher::stop()
{
    m_stop = true;
}

void SystemResourceWatcher::detectMemory()
{
    meminfo();
    uint mbMainAvailable =  kb_main_available / (double)1024;
//    qDebug() << mbMainAvailable << m_resourceThrehold.value(ConfManager::Low) << m_resourceThrehold.value(ConfManager::Medium) << m_resourceThrehold.value(ConfManager::High);
    if (mbMainAvailable > m_resourceThrehold.value(ConfManager::Low)) {
        return;
    }
    if (mbMainAvailable <= m_resourceThrehold.value(ConfManager::Low) &&
           mbMainAvailable > m_resourceThrehold.value(ConfManager::Medium)) {
        Q_EMIT ResourceThresholdWarning("Memory", 1);
        return;
    }

    if (mbMainAvailable <= m_resourceThrehold.value(ConfManager::Medium) &&
           mbMainAvailable > m_resourceThrehold.value(ConfManager::High)) {
        Q_EMIT ResourceThresholdWarning("Memory", 2);
        return;
    }

    if (mbMainAvailable <= m_resourceThrehold.value(ConfManager::High)) {
        Q_EMIT ResourceThresholdWarning("Memory", 3);
    }
}

void SystemResourceWatcher::start()
{
    m_timer->start(1000);
}
