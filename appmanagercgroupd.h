/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPMANAGERCGROUPD_H
#define APPMANAGERCGROUPD_H

#include <QObject>
#include <QMap>
#include <QVariant>

struct cgroup;
class SystemResourceWatcher;
class AppManagerCgroupd : public QObject
{
    Q_OBJECT
public:
    explicit AppManagerCgroupd(QObject *parent = nullptr);

    QList<int> pids(const QString &cgroupName);
    void thawAllProcesses(bool walk = false);

public Q_SLOTS:
    QMap<QString, QVariant> InitCGroup(const QString &rootPath);

    QMap<QString, QVariant> CreateProcessCGroup(const QString &appId, int pid);

    QMap<QString, QVariant> CreateProcessCGroup(const QString &appId, const QList<int> &pids);

    QMap<QString, QVariant> DeleteProcessCGroup(const QString &cgroupName);

    QMap<QString, QVariant> SetProcessCGroupResourceLimit(const QString &cgroupName, 
                                                          const QString &attrName,
                                                          int value);
    QMap<QString, QVariant> CGroupNameWithPid(int pid);

    QMap<QString, QVariant> PidsOfCGroup(const QString &cgroupName);

Q_SIGNALS:
    void ResourceThresholdWarning(const QString &resource,
                                  const QString &shareTime,
                                  const QString &threshold,
                                  const QString &avg);

private:
    bool initCgroup(const QString &rootPath);
    cgroup *createCgroup(const QString &cgroupName, const QStringList &controllers = QStringList());

    QMap<QString, QVariant> initResult();
    void thawProcess(struct cgroup_file_info *info, char *root);

private:
    bool m_cgroupInited;
    QString m_cgroupInitErrMsg;
    QStringList m_createdCGroupNames;
    QStringList m_frozenCGroups;

    // user.slice/user-1000.slice/
    QString m_rootCgroup;
    const QStringList kControllerNames = { 
        "cpu", "memory", "io", "pids"
    };

    const QString kResult = "result";
    const QString kErrMsg = "errorMessage";
};

#endif // APPMANAGERCGROUPD_H
