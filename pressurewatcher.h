/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PRESSUREWATCHER_H
#define PRESSUREWATCHER_H

#include <QObject>
#include <QJsonObject>
#include <QMap>
#include "resourcewatcher.h"
#include "confmanager.h"

class PressureWatcher : public ResourceWatcher
{
    Q_OBJECT
public:
    explicit PressureWatcher(QObject *parent = nullptr);

public Q_SLOTS:
    void start() override;
    void stop() override;

Q_SIGNALS:
    void finished();

private:
    struct PressureTrigger {
        ConfManager::Resource resource;
        ConfManager::ResourceUrgency urgency;
        QString trigger;
        QString fileInterface;
        int fd = -1;
    };

private:
    void initTriggers();
    PressureTrigger trigger(int fd);
    void handleWarningMessage(QMap<ConfManager::Resource, ConfManager::ResourceUrgency> triggerUrgency);

private:
    bool m_stop;
    QList<PressureTrigger> m_triggers;
    QMap<ConfManager::Resource, ConfManager::ResourceUrgencyThrehold> m_resourceThreholds;
};

#endif // PRESSUREWATCHER_H
