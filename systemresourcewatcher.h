/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SYSTEMRESOURCEWATCHER_H
#define SYSTEMRESOURCEWATCHER_H

#include <QObject>
#include <QJsonObject>
#include <QMap>
#include <QQueue>
#include "resourcewatcher.h"
#include "confmanager.h"

class QTimer;
class SystemResourceWatcher : public ResourceWatcher
{
    Q_OBJECT
public:
    explicit SystemResourceWatcher(QObject *parent = nullptr);

public Q_SLOTS:
    void start();
    void stop();

private Q_SLOTS:
    void detectMemory();

private:
    bool m_stop;
    QTimer *m_timer;
    ConfManager::ResourceUrgencyThrehold m_resourceThrehold;
};

#endif // SYSTEMRESOURCEWATCHER_H
