cmake_minimum_required(VERSION 3.14)

project(kylin-process-manager LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core DBus)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core DBus)

set (SRC_Sources
   main.cpp
   logger.cpp
   processmanager.cpp
   appmanagercgroupd.cpp
   resourcewatcher.cpp
   pressurewatcher.cpp
   systemresourcewatcher.cpp
   confmanager.cpp
   processreclaimer.cpp
)

set (SRC_Headers
    logger.h
    processmanager.h
    appmanagercgroupd.h
    resourcewatcher.h
    pressurewatcher.h
    systemresourcewatcher.h
    confmanager.h
    processreclaimer.h
    common.h)

qt5_add_dbus_adaptor(SRC_Sources configs/com.kylin.ProcessManager.xml processmanager.h ProcessManager processmanagerservice ProcessManagerService)

add_executable(kylin-process-manager
  ${SRC_Sources}
  ${SRC_Headers}
)

set(QAPPLICATION_CLASS QCoreApplication CACHE STRING "Inheritance class for SingleApplication")
add_subdirectory(third-party/SingleApplication)
target_link_libraries(kylin-process-manager SingleApplication::SingleApplication)

target_link_libraries(kylin-process-manager
    Qt${QT_VERSION_MAJOR}::Core
    Qt${QT_VERSION_MAJOR}::DBus
    cgroup
    procps
    spdlog)

#install(TARGETS kylin-app-cgroupd
#    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

install(TARGETS kylin-process-manager DESTINATION /usr/bin)
