/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <QObject>

class AppManagerCgroupd;
class ResourceWatcher;
class ProcessReclaimer;
class ProcessManager : public QObject
{
    Q_OBJECT
public:
    explicit ProcessManager(QObject *parent = nullptr);
    void startWatchers();
    void thawAllProcesses(bool walk = false);

    QMap<QString, QVariant> InitCGroup(const QString &rootPath);

    QMap<QString, QVariant> CreateProcessCGroup(const QString &appId, int pid);

    QMap<QString, QVariant> CreateProcessCGroup(const QString &appId, const QList<int> &pids);

    QMap<QString, QVariant> DeleteProcessCGroup(const QString &cgroupName);

    QMap<QString, QVariant> SetProcessCGroupResourceLimit(const QString &cgroupName,
                                                          const QString &attrName,
                                                          int value);

    QMap<QString, QVariant> CGroupNameWithPid(int pid);

    QMap<QString, QVariant> PidsOfCGroup(const QString &cgroupName);

    QMap<QString, QVariant> ReclaimProcesses(const QList<int> &pids);

    QMap<QString, QVariant> ReclaimProcesses(const QString &cgroup);

    QStringList Features() const;

Q_SIGNALS:
    void ResourceThresholdWarning(const QString &resource, int level);

private:
    void startWatcherThread(ResourceWatcher *watcher, QThread *thread);

private:
    AppManagerCgroupd *m_appMgrCgroupd;
    ProcessReclaimer *m_processReclaimer;
};

#endif // PROCESSMANAGER_H
