/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESSRECLAIMER_H
#define PROCESSRECLAIMER_H

#include <QObject>
#include "common.h"

class ProcessReclaimer : public QObject
{
    Q_OBJECT
public:
    explicit ProcessReclaimer(QObject *parent = nullptr);
    static bool isAvailable();
    common::DBusResult reclaimProcesses(const QList<int> pids);

signals:

private:
    bool m_isAvailable;
};

#endif // PROCESSRECLAIMER_H
