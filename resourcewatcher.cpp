/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "resourcewatcher.h"

ResourceWatcher::ResourceWatcher(QObject *parent)
    : QObject{parent}
    , m_detectEffectiveNumber(5)
{

}

bool ResourceWatcher::thresholdWarning(int urgency)
{
    m_detectValues.enqueue(urgency);
    if (m_detectValues.size() > m_detectEffectiveNumber) {
        m_detectValues.dequeue();
    }
    if (m_detectValues.size() < m_detectEffectiveNumber) {
        return false;
    }

    int result = m_detectValues.first();
    for (const auto &val : m_detectValues) {
        result &= val;
    }
    return result;    
}
