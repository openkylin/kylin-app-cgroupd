/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_H
#define COMMON_H

#include <QMap>
#include <QVariant>
#include <QString>

#define CGROUP_FREEZE "cgroup.freeze"
namespace common {

typedef QMap<QString, QVariant> DBusResult;
static const char *kDbusResult = "result";
static const char *kDbusErrMsg = "errorMessage";

static DBusResult initDbusResult()
{
    DBusResult result;
    result[kDbusResult] = "";
    result[kDbusErrMsg] = "";
    return result;
}

template<typename T>
class Singleton {
public:
    static T& GetInstance() {
        static T instance;
        return instance;
    }
protected:
    virtual ~Singleton() {}
    Singleton() {}
    Singleton(const Singleton&) {}
    Singleton& operator =(const Singleton&) {}
};

}

#endif // COMMON_H
